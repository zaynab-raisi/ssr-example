import Vue from 'vue';
import Vuex from 'vuex';
import { fetchSources } from "./api/index";

Vue.use(Vuex);

export function createStore(){
	return new Vuex.Store({
		state: {
			articles: {}
		},
		actions: {
			fetchSources({commit}){
				return fetchSources().then((sources) => {
					commit('setSources', sources);
				});
			}
		},
		mutations: {
			setSources(state, sources){
				state.articles = sources.articles
			}
		}
	})
}
