const axios = require('axios');
const NEWS_API = "ec1eaeb9ca5c45528412e996ca3c859b";

function fetch(url, params = null){
	
	const cache = axios.cachedItems;	
	let key;
	
	if(params) {
		key = url + '_' + params.source;
	}else {
		key = url;
	}
	
	if(cache && cache.has(key)){
		return Promise.resolve(cache.get(key));
	}else {
		return new Promise((resolve, reject) => {
			axios.get(url, {
				params: params
			}).then((res) => {
				
				if(res.data.status === "ok"){
					cache && cache.set(key, res.data);
					resolve(res.data);
				}else{
					reject("News API error: " + res.data.message);
				}
				
			}).catch((err) => {
				reject("Axios issue: " + err)
			})
		});
	}
}

export function fetchSources() {
	return fetch('https://newsapi.org/v2/top-headlines', { category: 'technology', apiKey: NEWS_API });
}

